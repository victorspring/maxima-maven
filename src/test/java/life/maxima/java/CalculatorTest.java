package life.maxima.java;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class CalculatorTest {

    @ParameterizedTest
    @CsvSource(value = {"2, 2, 4", "5, 5, 10", "2, 5, 7"})
    public void testAdd(int a, int b, int result) {
        Assertions.assertEquals(result, Calculator.add(a, b));
    }

    @Test
    public void assertNull() {
        Object o = new Object();
        Assertions.assertNotNull(o);
    }

    @Test
    public void assertThrows() {
        Assertions.assertThrows(NullPointerException.class, () -> {
            String s = null;
            s.toUpperCase();
        });
    }


}
