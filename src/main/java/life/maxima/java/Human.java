package life.maxima.java;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
//@Getter
//@Setter
//@ToString
public class Human {

    private String name;
    private int age;

}
