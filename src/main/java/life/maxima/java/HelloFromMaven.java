package life.maxima.java;

public class HelloFromMaven {

    public static void main(String[] args) {
//        System.out.println("Hello, " + args[0] + "!");

        Human human = new Human();
        Human human2 = new Human("Ivan", 20);

        human.setName("Victor");
        human.setAge(27);

        Human human3 = Human.builder()
                .name("Masha")
                .age(30)
                .build();

        System.out.println(human);
        System.out.println(human2);
        System.out.println(human3);
    }
}
